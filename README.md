To prepare the local environment:

    make dev

To run tests with test coverage reports:

    make test
