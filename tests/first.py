import unittest
from project1 import Conversion


class ConversionTest(unittest.TestCase):
    def setUp(self):
        self.conv = Conversion()

    def test_int2str(self):
        self.assertEqual(self.conv.int2str(1), '1')
        self.assertEqual(self.conv.int2str(1, 2), ['1', '2'])

    def test_dict2repr(self):
        self.assertEqual(self.conv.dict2repr({
            "name": "Mary",
            "age": 36
        }), "{'age': 36, 'name': 'Mary'}")
