import unittest
from project1 import ExtraCalc


class ExtraCalcTest(unittest.TestCase):
    def setUp(self):
        self.extra = ExtraCalc(7)

    def test_how_many_items(self):
        self.assertEqual(self.extra.how_many_items(range(7)), 49) # 7 * 7
