from setuptools import setup

setup(
    name='project1',
    version='0.0.1',
    long_description=__doc__,
    packages=['project1'],
    include_package_data=True,
    zip_safe=False,
    scripts=['update_dashboard.py']
)
