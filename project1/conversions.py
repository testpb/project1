class Conversion(object):
    def int2str(self, *values):
        strings = map(str, values)

        if len(strings) == 0:
            return None
        elif len(strings) == 1:
            return strings[0]
        else:
            return strings

    def dict2repr(self, d):
        return repr(d)
