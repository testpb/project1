class ExtraCalc(object):
    rate = int()

    def __init__(self, rate=1):
        self.rate = rate

    def how_many_items(self, items):
        return len(items) * self.rate
