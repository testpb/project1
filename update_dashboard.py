#!/usr/bin/env python
"""
This script should a separated package used by many different repositories.
"""

import pusher
import argparse
import os
import sys
from coverage import coverage


TEST_COVERAGE_FILE = '.coverage'
PUSHER_KEY = '6c474bb2a3c235a382b0'
PUSHER_SECRET = '94f3b4a6e251e9526b3e'
PUSHER_APPID = '112269'
PUSHER_CHANNEL = 'testpb'
PUSHER_EVENT = 'build_update'


def get_coverage(filename, include=None):
    cov = coverage(filename)
    cov.load()
    if include: include = include.split(",")
    return int(round(cov.xml_report(include=include)))


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--repository", help='Determines the repository name for tracking')
    #parser.add_argument("--build", help='Build ID from Buildbot')
    parser.add_argument("-b", "--branch", help='Which branch this update is about', default='master')
    parser.add_argument("-m", "--message", help='A short free description', default='')
    parser.add_argument("-t", "--result", help='Test result - passed or failed')
    parser.add_argument("-i", "--coverage", help='List separated by commas with folders to report coverage')
    return parser.parse_args()


def send_to_pusher(args):
    if args.result is not None and os.path.exists(TEST_COVERAGE_FILE) and args.coverage:
        test_coverage = get_coverage(TEST_COVERAGE_FILE, args.coverage)
    else:
        test_coverage = None

    data = {
        "repository": args.repository,
        #"build": args.build,
        "branch": args.branch,
        "message": args.message,
        "result": args.result,
        "coverage": test_coverage,
    }
    p = pusher.Pusher(app_id=PUSHER_APPID, key=PUSHER_KEY, secret=PUSHER_SECRET)
    p[PUSHER_CHANNEL].trigger(PUSHER_EVENT, data)


if __name__ == '__main__':
    args = parse_arguments()
    send_to_pusher(args)

    if args.result == 'failed':
        sys.exit(1)
