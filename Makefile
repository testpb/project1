GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
SHELL := /bin/bash

dev:
	@virtualenv env
	@./env/bin/pip install -r requirements.txt
	@./env/bin/python update_dashboard.py -r project1 -m "Test started - preparing virtualenv" -b "$(GIT_BRANCH)"
	@./env/bin/python setup.py develop
	@./env/bin/python update_dashboard.py -r project1 -m "Ran setup.py develop" -b "$(GIT_BRANCH)"

test:
	@./env/bin/python update_dashboard.py -r project1 -m "Running tests" -b "$(GIT_BRANCH)"
	@echo "failed" > /tmp/tests_passed
	@./env/bin/coverage run tests/__init__.py || \
	 ./env/bin/python update_dashboard.py -r project1 -m "Tests ran" -b "$(GIT_BRANCH)" \
																				-t "failed" -i "project1/*"
	@./env/bin/coverage report -m --include=./project1/*
	@./env/bin/python update_dashboard.py -r project1 -m "Tests ran" -b "$(GIT_BRANCH)" \
																				-t "passed" -i "project1/*"
